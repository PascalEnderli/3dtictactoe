﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

public class Rotate_Cells : MonoBehaviour {
	private Leap.Controller controller;

	public bool withLeap;

	public float MouseHorizontalSpeed = 5.0F;
	public float MouseVerticalSpeed = 5.0F;

	private bool isRotating;

	void Start() {
		controller = FindObjectOfType<HandController> ().GetLeapController ();
		withLeap = true;
	}

	void Update() {

		if (withLeap) {
			var frame = controller.Frame ();

			Hand firstHand = frame.Hands [0];
			Hand secondHand = frame.Hands [1]; 
			Hand LeftHand = null;
			float strength = 0;

			//Choose left hand
			for (int i = 0; i < 2; i++) {
				if (firstHand.IsLeft)
					LeftHand = firstHand;
				if (secondHand.IsLeft)
					LeftHand = secondHand;
			}
			if (LeftHand != null) {
				strength = LeftHand.GrabStrength;
				Debug.Log (strength);
			}

			//Debug.Log (PVely);
			//Debug.Log (PVelz);

			if (strength > 0.5) {
				isRotating = true;
			}

			if (strength < 0.5)
				isRotating = false;

			if (isRotating) {
				float h = -0.01F * firstHand.PalmVelocity.z;
				float v = -0.01F * firstHand.PalmVelocity.y;
				float w = -0.01F * firstHand.PalmVelocity.x;
				transform.Rotate (w, h, v);
			}
		}

		if (!withLeap) {

			if(Input.GetMouseButtonDown(0))
			{
				isRotating = true;
			}

			// Disable movements on button release
			if (!Input.GetMouseButton(0)) isRotating=false;

			if (isRotating) {
				float h = MouseHorizontalSpeed * Input.GetAxis("Mouse X");
				float v = MouseVerticalSpeed * Input.GetAxis("Mouse Y");
				transform.Rotate(v, h, 0);
			}

		}

	}
}