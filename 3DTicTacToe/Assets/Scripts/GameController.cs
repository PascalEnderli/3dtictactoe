﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	//---------------------------------------------------
	// Define Classes

	public class player
	{
		public int ID;					//1 or 2
		public string token;			//object name of coresponding token
		public player(int an_ID, string a_token) //constructor
		{
			ID=an_ID;
			token=a_token;
		}
	}

	public class cell
	{
		public Vector3 position;
		public float status;
		public string name;
	}

	public class token_position
	{
		public Vector3 actual_token_pos;		//position of active token in World KOS
		public cell nearest_cell;	//position of neaest cell in World KOS
		public float dist_to_nearest_cell;
	}



	//---------------------------------------------------
	//Init Variables

	public int n_free_cells;
	public cell[,,] playfield = new cell[3,3,3];
		
	token_position current_position = new token_position();
	token_position old_position = new token_position();

	player p1= new player(1,"token_p1");
	player p2= new player(2,"token_p2");
	player active_player;

	void Start()
	{
		if (1 == Random.Range (1, 3) )//3 is excl.
		{
			active_player = p1;
		}
		else
		{
			active_player = p2;
		}
		n_free_cells=GameObject.FindGameObjectsWithTag("cell").Length; 		// get n_cells
		current_position.actual_token_pos = GameObject.Find(active_player.token).transform.position;


		int dim = (int)Mathf.Pow(n_free_cells,1f/3f);   //must be a odd number

		for (int i =0; i < dim; i++)
		{
			for (int j =0; j < dim; j++)
			{
				for (int k =0; k < dim; k++)
				{
					
					GameObject.Find ("MyTrigger").transform.position.Set (i, j, k);
					playfield[i,j,k].position.Set(i,j,k);
					//playfield [i, j, k].name = other.get_name();
					print(playfield [i, j, k].name);
				}
			}
		}





	}
		













	//---------------------------------------------------
	//Call Methods once per frame

	void Update () //sequence of called Methods
	{
		update_positions();			
		token_is_moving();
		print(Trigger.name);
	}


	//---------------------------------------------------
	//Define Methods

	void update_positions()
	{
		old_position.actual_token_pos = current_position.actual_token_pos;
		current_position.actual_token_pos = GameObject.Find (active_player.token).transform.position;
	}

	bool token_is_moving()
	{
		if(0.0f == (current_position.actual_token_pos - old_position.actual_token_pos).sqrMagnitude )
		{
				return false;
		}

		else
		{
				return true;
		}
	}


}
